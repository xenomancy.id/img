# Your Favicon Package

This package was generated with [RealFaviconGenerator](https://realfavicongenerator.net/) [v0.16](https://realfavicongenerator.net/change_log#v0.16)

## Install instructions

To install this package:

Extract this package in <code>https://img.xenomancy.id/icons/</code>. For example, you should be able to access a file named <code>https://img.xenomancy.id/icons/favicon.ico</code>.

Insert the following code in the `head` section of your pages:

    <link rel="apple-touch-icon" sizes="180x180" href="https://img.xenomancy.id/icons/apple-touch-icon.png?v=alJ9yEr3LX">
    <link rel="icon" type="image/png" sizes="32x32" href="https://img.xenomancy.id/icons/favicon-32x32.png?v=alJ9yEr3LX">
    <link rel="icon" type="image/png" sizes="16x16" href="https://img.xenomancy.id/icons/favicon-16x16.png?v=alJ9yEr3LX">
    <link rel="manifest" href="https://img.xenomancy.id/icons/site.webmanifest?v=alJ9yEr3LX">
    <link rel="mask-icon" href="https://img.xenomancy.id/icons/safari-pinned-tab.svg?v=alJ9yEr3LX" color="#5bbad5">
    <link rel="shortcut icon" href="https://img.xenomancy.id/icons/favicon.ico?v=alJ9yEr3LX">
    <meta name="msapplication-TileColor" content="#666666">
    <meta name="msapplication-config" content="https://img.xenomancy.id/icons/browserconfig.xml?v=alJ9yEr3LX">
    <meta name="theme-color" content="#666666">

*Optional* - Check your favicon with the [favicon checker](https://realfavicongenerator.net/favicon_checker)